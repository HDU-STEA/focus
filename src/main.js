import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router';
import './assets/index.css'
import reservation from "./components/reservation";
import success from "./components/success";
import order from "./components/order";
import content from "./components/content";

Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.config.productionTip = false

Vue.component('success', success)
Vue.component('reservation', reservation)
Vue.component('order', order)
Vue.component('content', content)

const routes = [
  {
    path: '/success', component: success, meta: {
      // 页面标题title
      title: '预约成功',
      keepAlive:false
    }
  },
  {
    path: '/', component: reservation, name: "reservation", meta: {
      // 页面标题title
      title: '填写预约单',
      keepAlive:false
    }
  },
  {
    path: '/order', component: order, name: "order", meta: {
      // 页面标题title
      title: '查询预约',
      keepAlive:true
    }
  },
  {
    path: '/content', component: content, name: "content", meta: {
      // 页面标题title
      title: '查询预约',
      keepAlive:false
    }
  },
]

const router = new VueRouter({
  routes // (缩写) 相当于 routes: routes
})



new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
